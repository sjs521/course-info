# CSE 262 - Bulletin Board

## Homework

- [ ] 08/31 - [Homework 1](https://gitlab.com/lehigh-cse262/fall-2020/assignments/homework-1) - Guessing game in Rust
- [ ] 08/31 - [Homework 0](https://gitlab.com/lehigh-cse262/fall-2020/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab

## Readings

### Week 1

- [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6
- [RustConf 2020 Keynote](https://youtu.be/ESXMg9OzWrQ?t=1246) - [Slides](https://docs.google.com/presentation/d/e/2PACX-1vSA_hS_o_sOgosYSbT5MnasFBSYxTLCJWjjTX8lqoKm5P8AqAp9wSIa9uYzfd60yFrm1DCjU_dI3AxC/pub)
- [Rust for Non Systems Programmers](https://youtu.be/ESXMg9OzWrQ?t=23842) - [Slides](https://becca.ooo/rustconf/2020/)
- [The Rust Book](https://doc.rust-lang.org/stable/book/ch01-00-getting-started.html) - Chapters 1, 2